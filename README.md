# todo-viewer
An npm package that prints out todos to the console. When running the command, the glob must be in quotes.
## Install
To install the package globally, run:
```bash
npm i -g todo-viewer
```
Or to install the package locally, run:
```bash
npm i -D todo-viewer
```

## Usage
When writing todos in your project, write them in the form:
```
// TODO: Lorem ipsum
```

If the package has been installed globally, run:
```bash
todo-viewer <file>
```
or add a script to your `package.json` like:
```json
"scripts": {
	"todo": "todo-viewer \"*.js\""
}
```
The `<file>` argument is a **glob**. This will log the todos to the console from all the files that the glob matches. `<file>` **must** be in **quotes** otherwise bash will match the glob before the package.
