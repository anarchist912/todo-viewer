// Remove the node dependencies from the list of files
module.exports = (files) => {
  return new Promise((resolve, reject) => {
    let newFiles = []
    files.forEach(file => {
      const split = file.split('/')
      if (split[0] === 'node_modules' || split[1] === 'node_modules') return
      newFiles.push(file)
    })
    resolve(newFiles)
  })
}
