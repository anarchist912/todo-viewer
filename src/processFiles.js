// Checks files for todos
module.exports = (files) => {
  let found = false
  files.forEach(file => {
    let done = false
    const lines = file.text.split('\n')
    lines.forEach(line => {
      if (line.includes('// TODO:')) {
        if (!done) console.log('\x1b[1m', file.filename.substring(2) + ':')
        console.log('\x1b[0m - ' + line.trim().substring(9))
        found = true
        done = true
      }
    })
  })

  if (!found) console.log('No todos found. All up to date!')
}
