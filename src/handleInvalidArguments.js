const packagejson = require('../package.json')

// Handle invalid arguments
module.exports = (path) => {
  const usage = `Usage: ${packagejson.name} <files>\n` +
    `Filenames must be in quotes\n` +
    `\t--help\tShows information and commands`
  let shouldEnd = false
  switch (path) {
    case undefined:
      console.log(usage)
      shouldEnd = true
      break
    case '--help':
      console.log(`\x1b[1mtodo-viewer v${packagejson.version}\n` +
        `\x1b[0m${usage}\n\n` +
        `Created by Jake Gore:\n` +
        `https://github.com/JakeGore/todo-viewer`)
      shouldEnd = true
      break
  }
  if (shouldEnd) process.exit()
}
