const fs = require('fs')

// Read all the files and puts them into an array
module.exports = (files) => {
  return new Promise((resolve, reject) => {
    let allFiles = []
    files.forEach((file, key) => {
      fs.readFile(file, 'utf-8', (err, docs) => {
        if (err) reject(new Error('An error occurred'))
        allFiles.push({
          filename: file,
          text: docs
        })
        if (key === files.length - 1) resolve(allFiles)
      })
    })
  })
}
