const removeNodeModules = require('../src/removeNodeModules')

describe('Remove Node Modules', () => {
  it('Multiple choices w/ dot prefix', () => {
    const testArr = [
      './node_modules/test/test',
      './test/test'
    ]
    removeNodeModules(testArr)
      .then(res => expect(res).toEqual([
        './test/test'
      ]))
  })

  it('Multiple choices w/o dot prefix', () => {
    const testArr = [
      'node_modules/test/test',
      'test/test'
    ]
    removeNodeModules(testArr)
      .then(res => expect(res).toEqual([
        'test/test'
      ]))
  })

  it('Single choice w/ dot prefix', () => {
    const testArr = [
      './node_modules/test'
    ]
    removeNodeModules(testArr)
      .then(res => expect(res).toEqual([]))
  })

  it('Single choice w/o dot prefix', () => {
    const testArr = [
      'node_modules/test'
    ]
    removeNodeModules(testArr)
      .then(res => expect(res).toEqual([]))
  })
})
