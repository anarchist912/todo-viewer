const processGlob = require('../src/processGlob')

describe('Process Glob', () => {
  it('Test for app.js in root', () => {
    processGlob('*.js')
      .then(res => expect(res).toEqual([
        './app.js'
      ]))
  })

  it('Test for *.js in ./tests/processGlob w/ dot prefix', () => {
    processGlob('./tests/processGlob/*.js')
      .then(res => expect(res).toEqual([
        './tests/processGlob/test1.js',
        './tests/processGlob/test2.js'
      ]))
  })

  it('Test for *.js in ./tests/processGlob w/o dot prefix', () => {
    processGlob('tests/processGlob/*.js')
      .then(res => expect(res).toEqual([
        './tests/processGlob/test1.js',
        './tests/processGlob/test2.js'
      ]))
  })
})
