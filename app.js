#! /usr/bin/env node

const handleInvalidArguments = require('./src/handleInvalidArguments')
const processGlob = require('./src/processGlob')
const removeNodeModules = require('./src/removeNodeModules')
const concatFiles = require('./src/concatFiles')
const processFiles = require('./src/processFiles')

// Store the glob that was passed in by the user
const path = process.argv[2]
handleInvalidArguments(path)

processGlob(path)
  .then(removeNodeModules)
  .then(concatFiles)
  .then(processFiles)
  .catch(console.log)
